package me.ijauradunbi.hehehe.di

import android.content.Context
import me.ijauradunbi.hehehe.interactor.HeheheInteractor
import me.ijauradunbi.hehehe.presenter.SomethingPresenter
import me.ijauradunbi.hehehe.ui.view.SomethingView

/**
 * Created on 24/09/2016.
 */

class SomethingModule(val context: Context,
                      val somethingView: SomethingView) {
    fun provideInteractor(): HeheheInteractor {
        return HeheheInteractor()
    }

    fun provideSomethingPresenter(): SomethingPresenter {
        return SomethingPresenter(somethingView, provideInteractor())
    }
}
