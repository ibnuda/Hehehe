package me.ijauradunbi.hehehe.model

/**
 * Created on 05/09/2016.
 */
data class Story(
        val by: String,
        val descendants: Int,
        val id: Int,
        val kids: List<Int>,
        val score: Int,
        val time: Int,
        val title: String,
        val url: String
) {
    companion object {
        val NAME: String = "Story"
        val NUMBER: Int = 4
    }
}
