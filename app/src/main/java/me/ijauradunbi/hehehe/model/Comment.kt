package me.ijauradunbi.hehehe.model

/**
 * Created on 05/09/2016.
 */

data class Comment(
        val by: String,
        val id: Int,
        val kids: List<Int>,
        val parent: Int,
        val text: String,
        val time: Int
) {
    companion object {
        val NAME: String = "Comm"
        val NUMBER: Int = 1
    }
}
