package me.ijauradunbi.hehehe.model

/**
 * Created on 05/09/2016.
 */

data class Poll(
        val by: String,
        val descendants: Int,
        val id: Int,
        val kids: List<Int>,
        val parts: List<Int>,
        val score: Int,
        val time: Int,
        val title: String
) {
    companion object {
        val NAME: String = "Poll"
        val NUMBER: Int = 3
    }
}
