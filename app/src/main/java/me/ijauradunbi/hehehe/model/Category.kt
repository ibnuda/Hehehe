package me.ijauradunbi.hehehe.model

/**
 * Created on 21/09/2016.
 */

data class Category(val number: Int) {
    fun name(number: Int): String {
        return when (number) {
            0 -> Ask.NAME
            1 -> Comment.NAME
            2 -> Job.NAME
            3 -> Poll.NAME
            4 -> Story.NAME
            else -> ""
        }
    }
}
