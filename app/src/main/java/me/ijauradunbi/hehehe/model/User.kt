package me.ijauradunbi.hehehe.model

/**
 * Created on 05/09/2016.
 */

data class User(
        val about: String,
        val created: Int,
        val delay: Int,
        val id: String,
        val karma: Int,
        val submitted: List<Int>
)
