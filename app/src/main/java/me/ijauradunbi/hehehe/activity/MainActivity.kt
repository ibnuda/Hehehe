package me.ijauradunbi.hehehe.activity

import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.AdapterView
import me.ijauradunbi.hehehe.R
import me.ijauradunbi.hehehe.di.SomethingModule
import me.ijauradunbi.hehehe.presenter.SomethingPresenter
import me.ijauradunbi.hehehe.ui.adapter.CategoryPagerAdapter
import me.ijauradunbi.hehehe.ui.adapter.SomethingAdapter
import me.ijauradunbi.hehehe.ui.ui.activity.MainUI
import me.ijauradunbi.hehehe.ui.view.CategoryView
import me.ijauradunbi.hehehe.ui.view.SomethingView
import org.jetbrains.anko.*

class MainActivity : AppCompatActivity(),
        AdapterView.OnItemSelectedListener,
        SomethingView,
        CategoryView,
        AnkoLogger {
    private var viewPager: ViewPager? = null
    private var progressDialog: ProgressDialog? = null
    private var somethingAdapter: SomethingAdapter? = null
    lateinit var somethingPresenter: SomethingPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainUI().setContentView(this)
        setSupportActionBar(findViewById(R.id.id_toolbar) as Toolbar?)
        viewPager = findViewById(R.id.id_pager) as ViewPager?
        selectAdapterForActiveCategory(0)
        setupTab(viewPager)
        setupPresenter()
    }

    private fun setupPresenter() {
        somethingPresenter = SomethingModule(this, this).provideSomethingPresenter()
        somethingPresenter.createView(0)
    }

    private fun setupTab(viewPager: ViewPager?) {
        val tabLayout = findViewById(R.id.id_tab) as TabLayout?
        // somethingAdapter = SomethingAdapter(emptyList())
        tabLayout!!.setupWithViewPager(viewPager)
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        // selectAdapterForActiveCategory(p2)
        selectAdapterForActiveCategory(viewPager!!.currentItem)
    }

    override fun showLoading() {
        viewPager!!.visibility = View.GONE
        progressDialog = indeterminateProgressDialog(R.string.fetching, R.string.loading)
        progressDialog!!.show()
    }

    override fun hideLoading() {
        viewPager!!.visibility = View.VISIBLE
        progressDialog!!.dismiss()
    }

    override fun selectAdapterForActiveCategory(categoryNumber: Int) {
        viewPager!!.adapter = CategoryPagerAdapter(supportFragmentManager, categoryNumber)
    }

    override fun showSomethingList(stories: List<Int>) {
        // somethingAdapter!!.somethingList = stories
    }

}
