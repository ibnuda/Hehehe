package me.ijauradunbi.hehehe.network

import me.ijauradunbi.hehehe.interactor.HeheheInteractor
import me.ijauradunbi.hehehe.model.Story
import retrofit2.Call
import retrofit2.Response
import rx.Observable
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.lang.kotlin.observable
import rx.lang.kotlin.subscriber
import rx.schedulers.Schedulers

/**
 * Created on 22/09/2016.
 */
class HeheheService(val interactor: HeheheInteractor) {

    fun getStories(category: String) {

    }


    fun getStories(): Observable<List<Observable<Story>>> {
        return Observable.create { subscriber: Subscriber<in List<Observable<Story>>> ->
            val callResponse = interactor.getStories()
            val response = callResponse.execute()

            if (response.isSuccessful) {
                val listObservedStory: List<Observable<Story>> = response.body().map {
                    getStory(it)
                }
                subscriber.onNext(listObservedStory)
                subscriber.onCompleted()
            } else {
                subscriber.onError(Throwable(response.message()))
            }
        }
    }

    fun getStory(id: Int): Observable<Story> {
        return Observable.create { subscriber ->
            val callResponse: Call<Story> = interactor.getStory(id)
            val response: Response<Story> = callResponse.execute()

            if (response.isSuccessful) {
                val story: Story = response.body()
                subscriber.onNext(story)
                subscriber.onCompleted()
            } else {
                subscriber.onError(Throwable(response.message()))
            }
        }
    }

    fun anotherGetStories() {
        observable<List<Story>> { subscriber ->
        }
    }

    fun anotherGetStory(id: Int) {
        observable<Story> { subscriber ->

        }
    }
}
