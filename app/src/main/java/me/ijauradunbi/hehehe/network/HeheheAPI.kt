package me.ijauradunbi.hehehe.network

import me.ijauradunbi.hehehe.model.Story
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import rx.Observable

/**
 * Created on 19/09/2016.
 */

interface HeheheAPI {
    companion object {
        val BASE_URL: String = "https://hacker-news.firebaseio.com/v0/"
    }

    // Should I pick this function or the later?
    @GET("{category}stories.json")
    fun getStories(@Path("category") category: String): Call<List<Int>>

    @GET("topstories.json")
    fun getTopStories(): Call<List<Int>>

    @GET("newstories.json")
    fun getNewStories(): Call<List<Int>>

    @GET("beststories.json")
    fun getBestStories(): Call<List<Int>>

    @GET("askstories.json")
    fun getAskStories(): Call<List<Int>>

    @GET("showstories.json")
    fun getShowStories(): Call<List<Int>>

    @GET("jobstories.json")
    fun getJobStories(): Call<List<Int>>

    @GET("item/{id}.json")
    fun getStory(@Path("id") id: Int): Call<Story>

}
