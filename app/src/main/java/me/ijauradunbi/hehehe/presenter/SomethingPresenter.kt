package me.ijauradunbi.hehehe.presenter

import android.util.Log
import me.ijauradunbi.hehehe.interactor.HeheheInteractor
import me.ijauradunbi.hehehe.ui.view.SomethingView
import retrofit2.Call
import retrofit2.Response
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription

class SomethingPresenter(val somethingView: SomethingView,
                         val interactor: HeheheInteractor) {
    val subscriptions: CompositeSubscription = CompositeSubscription()

    fun loadObservableStories(category: String): Observable<List<Int>> {
        return Observable.create { subscriber ->
            val callResponse: Call<List<Int>> = interactor.getStories(category)
            val response: Response<List<Int>> = callResponse.execute()

            if (response.isSuccessful) {
                val arrayResponse: List<Int> = response.body().take(30)
                // Log.d(SomethingPresenter::class.java.canonicalName!!, "response is successful and here's the result : " + arrayResponse.toString())
                subscriber.onNext(arrayResponse)
                subscriber.onCompleted()
            } else {
                subscriber.onError(Throwable(response.message()))
            }
        }
    }

    fun requestStories(category: String) {
        val subscription = loadObservableStories(category)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    retrievedList: List<Int> ->
                    somethingView.showSomethingList(retrievedList)
                },
                {
                    err: Throwable ->
                    somethingView.showSomethingList(emptyList())
                }
        )
        subscriptions.add(subscription)
    }

    fun createView(category: Int) {
        Log.d(SomethingPresenter::class.java.canonicalName!!, category.toString())
        requestStories(getCategoryName(category))
    }

    fun getCategoryName(category: Int): String {
        return when(category) {
            0 -> "ask"
            1 -> "top"
            2 -> "job"
            3 -> "best"
            else -> "new"
        }
    }
}