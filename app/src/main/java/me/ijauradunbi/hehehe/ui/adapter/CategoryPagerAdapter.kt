package me.ijauradunbi.hehehe.ui.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.util.Log
import me.ijauradunbi.hehehe.ui.fragment.SomethingFragment

/**
 * Created on 21/09/2016.
 */

class CategoryPagerAdapter(fragmentManager: FragmentManager,
                           val category: Int):
        FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        // going to return a fragment layout that contains
        // recycler view.
        // return EmptyFragment.newInstance()
        // Log.d(CategoryPagerAdapter::class.java.canonicalName!!, "position: " + position)
        return SomethingFragment.newInstance(position)
    }

    override fun getCount(): Int {
        return 4
    }

    override fun getPageTitle(position: Int): CharSequence {
        return getCategoryName(position)
    }

    fun getCategoryName(no: Int): String {
        return when(no) {
            0 -> "ask"
            1 -> "top"
            2 -> "job"
            3 -> "best"
            else -> "new"
        }
    }
}
