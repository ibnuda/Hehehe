package me.ijauradunbi.hehehe.ui.ui

import android.support.v4.app.Fragment
import android.view.Gravity
import android.view.View
import me.ijauradunbi.hehehe.R
import org.jetbrains.anko.*

class EmptyUI(): AnkoComponent<Fragment> {
    override fun createView(ui: AnkoContext<Fragment>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent, height = matchParent)
                gravity = Gravity.CENTER
                textView(R.string.nothing_here)
            }
        }
    }

}
