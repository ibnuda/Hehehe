package me.ijauradunbi.hehehe.ui.view

import android.content.Intent
import me.ijauradunbi.hehehe.model.Category

/**
 * Created on 21/09/2016.
 */

interface CategoryView {
    // fun showCategory(category: List<Category>)
    fun selectAdapterForActiveCategory(categoryNumber: Int)
    fun showLoading()
    fun hideLoading()
    fun startActivityForResult(intent: Intent, requestCode: Int)
}
