package me.ijauradunbi.hehehe.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import me.ijauradunbi.hehehe.R
import me.ijauradunbi.hehehe.ui.ui.SomethingItemUI
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find
import java.util.*

class SomethingAdapter(var list: List<Int>): RecyclerView.Adapter<SomethingAdapter.Holder>() {
    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: Holder?, position: Int): Unit = holder!!.bind(list[position])

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): Holder =
            Holder(SomethingItemUI().createView(AnkoContext.create(parent!!.context, parent)))

    var somethingList: List<Int> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    class Holder(view: View): RecyclerView.ViewHolder(view) {
        val valueId: TextView = itemView.find(R.id.id_something)
        fun bind(value: Int) {
            valueId.text = value.toString()
        }
    }
}