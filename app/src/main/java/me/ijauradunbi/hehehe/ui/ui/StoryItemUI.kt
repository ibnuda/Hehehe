package me.ijauradunbi.hehehe.ui.ui

import android.view.View
import android.view.ViewGroup
import me.ijauradunbi.hehehe.R
import org.jetbrains.anko.*

// this class used as the container of the story information.
// this class itself contained in the StoryListUI class.
class StoryItemUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                textView {
                    id = R.id.id_story_title
                    textSize = 14f
                }
                textView {
                    id = R.id.id_story_by
                    textSize = 14f
                }
                textView {
                    id = R.id.id_story_score
                    textSize = 14f
                }
            }
        }
    }

}
