package me.ijauradunbi.hehehe.ui.ui.activity

import android.app.Activity
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.support.design.widget.NavigationView
import android.view.View
import me.ijauradunbi.hehehe.R
import me.ijauradunbi.hehehe.activity.MainActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.navigationView
import org.jetbrains.anko.design.tabLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.drawerLayout
import org.jetbrains.anko.support.v4.viewPager

class MainUI : AnkoComponent<MainActivity> {
    override fun createView(ui: AnkoContext<MainActivity>): View {
        return with(ui) {
            verticalLayout {
                id = R.id.id_appbar

                appBarLayout {
                    toolbar {
                        id = R.id.id_toolbar
                        overflowIcon!!.colorFilter = PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY)
                    }

                    tabLayout {
                        id = R.id.id_tab
                        setTabTextColors(Color.LTGRAY, Color.WHITE)
                        setSelectedTabIndicatorColor(Color.RED)
                    }
                }

                viewPager {
                    id = R.id.id_pager
                }

                /*
                recyclerView {
                    id  = R.id.id_container_list
                }
                */
            }
        }
    }
}
