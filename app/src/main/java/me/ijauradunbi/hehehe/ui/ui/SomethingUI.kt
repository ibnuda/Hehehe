package me.ijauradunbi.hehehe.ui.ui

import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.ViewGroup
import me.ijauradunbi.hehehe.R
import me.ijauradunbi.hehehe.ui.adapter.SomethingAdapter
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

class SomethingUI(val listAdapter: SomethingAdapter): AnkoComponent<Fragment> {
    override fun createView(ui: AnkoContext<Fragment>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                recyclerView {
                    lparams(width = matchParent, height = wrapContent)
                    id = R.id.id_recycler_something
                    layoutManager = LinearLayoutManager(ctx)
                    adapter = listAdapter
                }
            }
        }
    }

}
