package me.ijauradunbi.hehehe.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.ijauradunbi.hehehe.ui.ui.EmptyUI
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import org.jetbrains.anko.support.v4.ctx

class EmptyFragment: Fragment() {
    companion object {
        fun newInstance(): EmptyFragment {
            return EmptyFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        Log.d(EmptyFragment::class.java.canonicalName!!, "EmptyFragment was here.")
        return EmptyUI().createView(AnkoContext.create(ctx, this))
    }
}
