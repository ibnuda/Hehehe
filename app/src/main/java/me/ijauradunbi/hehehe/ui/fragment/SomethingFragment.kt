package me.ijauradunbi.hehehe.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.ijauradunbi.hehehe.di.SomethingModule
import me.ijauradunbi.hehehe.presenter.SomethingPresenter
import me.ijauradunbi.hehehe.ui.adapter.SomethingAdapter
import me.ijauradunbi.hehehe.ui.ui.SomethingUI
import me.ijauradunbi.hehehe.ui.view.SomethingView
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.support.v4.ctx


class SomethingFragment: Fragment(), SomethingView {

    companion object {
        private val CATEGORY: String = "category"
        fun newInstance(category: Int): SomethingFragment {
            val arguments: Bundle = Bundle()
            arguments.putInt(CATEGORY, category)
            val fragment = SomethingFragment()
            fragment.arguments = arguments
            return fragment
        }
    }

    val somethingAdapter = SomethingAdapter(emptyList<Int>())
    lateinit var somethingPresenter: SomethingPresenter

    override fun showSomethingList(stories: List<Int>) {
        somethingAdapter.list = stories
        somethingAdapter.notifyDataSetChanged()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val category: Int = arguments.getInt(CATEGORY)
        // Log.d(SomethingFragment::class.java.canonicalName!!, category.toString())
        somethingPresenter = SomethingModule(context, this).provideSomethingPresenter()
        somethingPresenter.createView(category)
        return SomethingUI(somethingAdapter).createView(AnkoContext.create(ctx, this))
    }
}
