package me.ijauradunbi.hehehe.ui.ui

import android.view.View
import android.view.ViewGroup
import me.ijauradunbi.hehehe.R
import org.jetbrains.anko.*

class SomethingItemUI: AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                textView {
                    id = R.id.id_something
                    lparams(width = matchParent, height = wrapContent)
                }
            }
        }
    }
}
