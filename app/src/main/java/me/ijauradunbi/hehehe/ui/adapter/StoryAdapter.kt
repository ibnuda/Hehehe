package me.ijauradunbi.hehehe.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import me.ijauradunbi.hehehe.R
import me.ijauradunbi.hehehe.model.Story
import me.ijauradunbi.hehehe.ui.ui.StoryItemUI
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find

class StoryAdapter(var stories: List<Story>): RecyclerView.Adapter<StoryAdapter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): Holder {
        return Holder(StoryItemUI()
                .createView(AnkoContext
                        .create(parent!!.context, parent)))
    }

    override fun getItemCount(): Int {
        return stories.size
    }

    override fun onBindViewHolder(holder: Holder?, position: Int) {
        holder!!.bind(stories[position])
    }

    class Holder(view: View): RecyclerView.ViewHolder(view) {
        val storyBy: TextView = itemView.find(R.id.id_story_by)
        val storyScore: TextView = itemView.find(R.id.id_story_score)
        val storyTitle: TextView = itemView.find(R.id.id_story_title)
        fun bind(story: Story) {
            storyBy.text = story.by
            storyScore.text = story.score.toString()
            storyTitle.text = story.title
        }
    }
}
