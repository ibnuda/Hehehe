package me.ijauradunbi.hehehe.util

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

/**
 * Created on 21/09/2016.
 */

class InfiniteScrollListener(
        val func: () -> Unit,
        val layoutManager: LinearLayoutManager): RecyclerView.OnScrollListener(), AnkoLogger {

    private var previousTotal: Int = 0
    private var loading: Boolean = true
    private var visibleThreshold: Int = 2
    private var firstVisibleItem: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if (0 < dy) {
            visibleItemCount = recyclerView.childCount
            totalItemCount = layoutManager.itemCount
            firstVisibleItem = layoutManager.findFirstVisibleItemPosition()

            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false
                    previousTotal = totalItemCount
                }
            }

            if (!loading &&
                    (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold))
            {
                info("End of the road")
                func()
                loading = true
            }
        }
    }
}
