package me.ijauradunbi.hehehe.interactor

import android.util.Log
import me.ijauradunbi.hehehe.model.Story
import me.ijauradunbi.hehehe.network.HeheheAPI
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import rx.Observable

class HeheheInteractor {
    /*
    val heheheService: HeheheService by lazy {
        RestAdapter.Builder()
                .setEndpoint(HeheheService.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build()
                .create(HeheheService::class.java)
    }
    */

    val heheheAPI: HeheheAPI

    init {
        val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(HeheheAPI.BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
        heheheAPI = retrofit.create(HeheheAPI::class.java)
    }

    fun getStories(category: String): Call<List<Int>> {
        // Log.d(HeheheInteractor::class.java.canonicalName!!, "the category of the stories is: " + category)
        return heheheAPI.getStories(category.toLowerCase())
    }

    fun getStories(): Call<List<Int>> {
        return heheheAPI.getTopStories()
    }

    fun getStory(id: Int): Call<Story> {
        return heheheAPI.getStory(id)
    }
}
